package com.andromet.customlist;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {

    String[] names, locations;
    int[] images;
    ListView list;
    int index;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        list = findViewById(R.id.listView2);

        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        names = bundle.getStringArray("names");
        locations = bundle.getStringArray("locations");
        images = bundle.getIntArray("images");
        index = bundle.getInt("size");

        CustomAdapter ca = new CustomAdapter();
        list.setAdapter(ca);
    }

    class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return index;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @SuppressLint({"ViewHolder", "InflateParams"})
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            convertView = getLayoutInflater().inflate(R.layout.custom_list, null);
            ImageView imageView = convertView.findViewById(R.id.imageView);
            TextView textViewName = convertView.findViewById(R.id.textViewName);
            TextView textViewLocation = convertView.findViewById(R.id.textViewLocation);

            imageView.setImageResource(images[position]);
            textViewName.setText(names[position]);
            textViewLocation.setText(locations[position]);
            return convertView;
        }
    }
}
