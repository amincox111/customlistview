package com.andromet.customlist;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    EditText editTextName, editTextLocation;
    Button btnSubmit, btnList;
    ImageView imageView;
    String name, location;
    String[] names = new String[1000], locations = new String[1000];
    int[] images = new int[1000];
    int index = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextName = findViewById(R.id.editTextName);
        editTextLocation = findViewById(R.id.editTextLocation);

        imageView = findViewById(R.id.imageView);

        btnSubmit = findViewById(R.id.btnSubmit);
        btnList = findViewById(R.id.btnList);

        images[0] = R.drawable.amin1;
        images[1] = R.drawable.amin2;
        images[2] = R.drawable.amin3;

        btnSubmit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if(index >= 3) {
                    if (index % 3 == 0) {
                        images[index] = images[0];
                    } else if (index % 2 == 0) {
                        images[index] = images[1];
                    } else {
                        images[index] = images[2];
                    }
                }

                name = editTextName.getText().toString();
                location = editTextLocation.getText().toString();
                names[index] = name;
                locations[index] = location;
                index += 1;
                editTextName.setText("");
                editTextLocation.setText("");
            }
        });

        btnList.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, Main2Activity.class);
                intent.putExtra("names", names);
                intent.putExtra("locations", locations);
                intent.putExtra("images", images);
                intent.putExtra("size", index);
                startActivity(intent);
            }
        });
    }
}
